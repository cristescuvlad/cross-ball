#include <windows.h> ///Sleep()
#include <direct.h> ///AllocConsole
#include <math.h>  ///sqrt,pow
#include <SFML/Graphics.hpp> ///sf::RenderWindow
#include <SFML/Audio.hpp> ///sf::Music

#define WORLD_EXTENSION 40 ///marim granitele ferestrei pentru obiectele cerc

#define MOBS_NR 10
#define PLAYER_COL 228,56,40  /// intre 0 si 255



bool ciocnire(sf::CircleShape player, sf::CircleShape obj)   ///functia de ciocnire dintre player si un obiect
{
    double distance = sqrt(pow(player.getPosition().x-obj.getPosition().x,2)+pow(player.getPosition().y-obj.getPosition().y,2));
    if(distance <= player.getRadius()+obj.getRadius()*7/10) ///factor de corectie
        return 1;
    return 0;
}

int main()
{
    HWND stealth;                                   ///ascund consola
    AllocConsole();                                 ///
    stealth=FindWindowA("ConsoleWindowClass",NULL); ///
    ShowWindow(stealth,0);                          ///




    srand(time(NULL));  ///initializam variabila aleatoare




    sf::RenderWindow window(sf::VideoMode(800, 100), "Proiect Cristescu Vlad");  /// crearea ferestrei grafice

    sf::Music music;                            ///muzica de fundal
    if (!music.openFromFile("background.wav"))  ///
        return -1;                              ///
    music.setLoop(true);                        ///
    music.play();                               ///




    sf::CircleShape player; ///cream jucatorul = un cerc si ii atribuim diferite caracteristici
    player.setRadius(25);
    player.setFillColor(sf::Color(PLAYER_COL));
    player.setPosition(20,50);




    sf::CircleShape mobs[MOBS_NR];  ///cream un vector de obiecte de tip cerc
    for(int i=0;i<MOBS_NR;i++)
    {
        mobs[i].setRadius(20);
        mobs[i].setFillColor(sf::Color(rand()%254+1,rand()%254+1,rand()%254+1));
        mobs[i].setPosition((i+1)*window.getSize().x/(MOBS_NR+1),10);
    }
    int objVector[MOBS_NR][2];  ///vectorii de directie pt fiecare obiect
    for(int i=0;i<MOBS_NR;i++)
    {
        objVector[i][0] = 0;
        objVector[i][1] = rand()%4+1;
    }



    bool pause = 0; ///variabila ce pune pauza jocului




    while (window.isOpen())
    {
        sf::Event event;    /// variabila eveniment pentru manevrarea inputurilor de la tastatura/mouse
        while (window.pollEvent(event))  ///cat timp avem evenimente
        {
            if (event.type == sf::Event::Closed)  /// daca evenimentul este alt-f4 sau close
                window.close();

            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::P))
            {
                pause = !pause;
            }

            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::W) && !pause)
            {
                if(player.getPosition().y >= -player.getRadius())
                    player.move(0,-3);
            }

            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::A) && !pause)
            {
                if(player.getPosition().x >= 0)
                    player.move(-3,0);
            }

            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::S) && !pause)
            {
                if(player.getPosition().y < window.getSize().y-player.getRadius())
                    player.move(0,3);
            }

            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::D) && !pause)
            {
                if(player.getPosition().x < window.getSize().x-player.getRadius())
                    player.move(3,0);
            }

        } //while (window.pollEvent(event))






        window.clear(); ///golim fereastra

        if(!pause)
        {
            for(int i=0;i<MOBS_NR;i++)
            {
                if(ciocnire(player,mobs[i]))    ///verificam daca jucatorul s-a ciocnit cu vreo bila
                {
                    music.stop();

                    sf::Font font;
                    font.loadFromFile("contrast.ttf");
                    sf::Text text("AI PIERDUT",font);
                    text.setCharacterSize(50);
                    text.setColor(sf::Color::Red);
                    text.setPosition(window.getSize().x/4,0);

                    window.draw(text);
                    window.display();
                    Sleep(2000);
                    return 0;  //iesim din program
                }

                if(player.getPosition().x >= window.getSize().x - 2*mobs[0].getRadius())  ///daca jucatorul ajunge la final
                {
                    sf::Font font;
                    font.loadFromFile("contrast.ttf");
                    sf::Text text("AI CASTIGAT !!!",font);
                    text.setCharacterSize(50);
                    text.setColor(sf::Color::Red);
                    text.setPosition(window.getSize().x/4,0);

                    window.draw(text);
                    window.display();
                    Sleep(2000);
                    return 0;  //iesim din program
                }

                if(mobs[i].getPosition().y <= -WORLD_EXTENSION || mobs[i].getPosition().y+2*mobs[i].getRadius() >= window.getSize().y+2*WORLD_EXTENSION)
                {                                                                                           ///mentinem obiectele in fereastra extinsa a jocului
                    objVector[i][1]=-objVector[i][1];
                    mobs[i].move(objVector[i][0],objVector[i][1]);
                }
            }

            for(int i=0;i<MOBS_NR;i++)  ///miscam obiectele pe vectorii lor
                mobs[i].move(objVector[i][0],objVector[i][1]);



        } //if(!pause)

        window.draw(player);    ///desenam figurile
        for(int i=0;i<MOBS_NR;i++)
            window.draw(mobs[i]);

        window.display(); ///deseneaza ce am ales sa desenam cu .draw();

        Sleep(50); /// pauza in milisecunde

    }

    return 0; //sfarsitul programului
}
